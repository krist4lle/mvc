<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Products extends AbstractMigration
{
    public function change(): void
    {
        $this->table('products')
            ->addColumn('sku', 'string')
            ->addColumn('name', 'string')
            ->addColumn('price', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('type_id', 'integer')
            ->addColumn('type_value', 'string', ['limit' => 45])
            ->addIndex('sku', ['unique' => true])
            ->addForeignKey('type_id', 'types', 'id', ['update' => 'NO_ACTION', 'delete' => 'CASCADE'])
            ->create();
    }
}
