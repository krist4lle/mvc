<?php


use Phinx\Seed\AbstractSeed;

class ProductSeeder extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'TypeSeeder'
        ];
    }

    public function run()
    {
        $books = $this->createBooks();
        $dvds = $this->createDvds();
        $furniture = $this->createFurniture();
        $data = array_merge($books, $dvds, $furniture);
        shuffle($data);

        $products = $this->table('products');
        $products->insert($data)->saveData();
    }

    private function createBooks(): array
    {
        $namesBook = $this->namesBook();
        for ($i = 0; $i < count($namesBook); $i++) {
            $dataBooks[$i] = [
                'sku' => $this->sku(),
                'name' => $namesBook[$i],
                'price' => $this->price(),
                'type_id' => 2,
                'type_value' => (string)rand(1, 5),
            ];
        }

        return $dataBooks;
    }

    private function createDvds(): array
    {
        $namesDvd = $this->namesDvd();
        for ($i = 0; $i < count($namesDvd); $i++) {
            $dataDvds[$i] = [
                'sku' => $this->sku(),
                'name' => $namesDvd[$i],
                'price' => $this->price(),
                'type_id' => 1,
                'type_value' => (string)rand(1024, 10240),
            ];
        }

        return $dataDvds;
    }

    private function createFurniture(): array
    {
        $namesFurniture = $this->namesFurniture();
        for ($i = 0; $i < count($namesFurniture); $i++) {
            $dataFurniture[$i] = [
                'sku' => $this->sku(),
                'name' => $namesFurniture[$i],
                'price' => $this->price(),
                'type_id' => 3,
                'type_value' => rand(10, 100) . 'x' . rand(10, 100) . 'x' . rand(10, 100),
            ];
        }

        return $dataFurniture;
    }

    private function sku(): string
    {
        return rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
    }

    private function price(): float
    {
        return (float)rand(5, 300);
    }

    private function namesBook(): array
    {
        return [
            'Insight',
            'Way Ahead',
            'Super Minds',
            'Harry Potter',
            'Lord of the Rings',
            'English Plus',
            'Amelia Fang'
        ];
    }

    private function namesDvd(): array
    {
        return [
            'Avengers',
            'Batman',
            'Doctor Strange',
            'Mission Impossible',
            'Fast and Furious',
            'Star Wars',
            'John Wick'
        ];
    }

    private function namesFurniture(): array
    {
        return [
            'Stool',
            'Table',
            'TV',
            'Fridge',
            'Sofa',
            'Chair',
            'Desk',
            'Bed'
        ];
    }
}
