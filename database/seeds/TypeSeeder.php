<?php


use Phinx\Seed\AbstractSeed;

class TypeSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name' => 'DVD',
                'slug' => 'dvd',
            ],[
                'name' => 'Book',
                'slug' => 'book',
            ],[
                'name' => 'Furniture',
                'slug' => 'furniture',
            ]
        ];

        $types = $this->table('types');
        $types->insert($data)->saveData();
    }
}
