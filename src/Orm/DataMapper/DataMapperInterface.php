<?php

declare(strict_types=1);

namespace Src\Orm\DataMapper;

interface DataMapperInterface
{
    public function prepare(string $sqlQuery): self;

    public function bind(mixed $value): mixed;

    public function bindParameters(array $fields, bool $isSearch = false): self;

    public function bindValues(array $fields): \PDOStatement;

    public function bindSearchValues(array $fields): \PDOStatement;

    public function numRows(): int;

    public function execute(): void;

    public function result(): Object;

    public function results(): array;
}