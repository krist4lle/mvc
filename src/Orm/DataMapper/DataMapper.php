<?php

declare(strict_types=1);

namespace Src\Orm\DataMapper;

use Src\Base\BaseModel;
use Src\Database\Database;
use Src\Database\DatabaseInterface;
use Src\Orm\DataMapper\Exception\DataMapperException;

class DataMapper implements DataMapperInterface
{
    private DatabaseInterface $db;

    private \PDOStatement $statement;

    private string $model;

    public function __construct(string $model)
    {
        $this->db = new Database();
        $this->model = $model;
    }

    public function prepare(string $sqlQuery): self
    {
        $this->statement = $this->db->open()->prepare($sqlQuery);

        return $this;
    }

    public function bind(mixed $value): mixed
    {
        switch ($value) {
            case is_bool($value):
            case intval($value):
                $dataType = \PDO::PARAM_INT;
                break;
            case is_null($value):
                $dataType = \PDO::PARAM_NULL;
                break;
            default:
                $dataType = \PDO::PARAM_STR;
                break;
        }

        return $dataType;
    }

    public function bindParameters(array $fields, bool $isSearch = false): self
    {
        try {
            if (!$this->isEmpty($fields)) {
                $isSearch === false ? $this->bindValues($fields) : $this->bindSearchValues($fields);

                return $this;
            }
        } catch (DataMapperException $exception) {

            throw $exception;
        }
    }

    public function bindValues(array $fields): \PDOStatement
    {
        try {
            $this->isArray($fields);
            foreach ($fields as $key => $value) {
                $this->statement->bindValue("{$key}", $value, $this->bind($value));
            }

            return $this->statement;

        } catch (\PDOException $exception) {

            throw new DataMapperException($exception->getMessage());
        }
    }

    public function bindSearchValues(array $fields): \PDOStatement
    {
        try {
            $this->isArray($fields);
            foreach ($fields as $key => $value) {
                $this->statement->bindValue("{$key}", "%{$value}%", $this->bind($value));
            }

            return $this->statement;

        } catch (\PDOException $exception) {

            throw new DataMapperException($exception->getMessage());
        }
    }

    public function numRows(): int
    {
        return $this->statement->rowCount();
    }

    public function execute(): void
    {
        $this->statement->execute();
        $this->db->close();
    }

    public function result(object $object = null): object
    {
        $object = $object ?? $this->statement->fetch(\PDO::FETCH_OBJ);
        $model = $this->createModel();
        if (is_array($object) || is_object($object)) {
            foreach ($object as $property => $value) {
                $model->$property = $value;
            }
        }

        return $model;
    }

    public function results(): array
    {
        $objects = $this->statement->fetchAll(\PDO::FETCH_OBJ);

        foreach ($objects as $i => $object) {
            $objects[$i] = $this->result($object);
        }

        return $objects;
    }

    private function isEmpty($value): bool
    {
        if (empty($value)) {

            throw new DataMapperException('Your argument should not be empty');
        }

        return false;
    }

    private function isArray($value): bool
    {
        if (!is_array($value)) {

            throw new DataMapperException('Your argument needs to be an array');
        }

        return true;
    }

    private function createModel(): object
    {
        $modelString = $this->model === 'BaseModel' ? BaseModel::class : "App\Model\\{$this->model}";

        return new $modelString();
    }
}
