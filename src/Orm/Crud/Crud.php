<?php

declare(strict_types=1);

namespace Src\Orm\Crud;

use Src\Orm\Crud\Exception\CrudException;
use Src\Orm\QueryBuilder\QueryBuilder;
use Src\Orm\QueryBuilder\QueryBuilderInterface;

class Crud implements CrudInterface
{
    protected QueryBuilderInterface $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = new QueryBuilder($this->model, $this->table);
    }

    public function query(): QueryBuilderInterface
    {
        return $this->queryBuilder;
    }

    public function create(array $data): void
    {
        $this->queryBuilder->insert($this->table, array_keys($data), array_values($data));
    }

    public function update(array $data): void
    {
        try {
            $this->queryBuilder->update($this->table, $data, $this->id);
        } catch (\Throwable $throwable) {

            throw new CrudException('Cannot update unidentified object');
        }

    }

    public function delete(): void
    {
        try {
            $this->queryBuilder->delete($this->table, $this->id);
        } catch (\Throwable $throwable) {

            throw new CrudException('Cannot delete unidentified object');
        }
    }
}