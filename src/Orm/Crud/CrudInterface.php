<?php

declare(strict_types=1);

namespace Src\Orm\Crud;

use Src\Orm\QueryBuilder\QueryBuilderInterface;

interface CrudInterface
{
    public function query(): QueryBuilderInterface;

    public function create(array $data): void;

    public function update(array $data): void;

    public function delete(): void;
}