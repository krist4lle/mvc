<?php

declare(strict_types=1);

namespace Src\Orm\QueryBuilder;

interface QueryBuilderInterface
{
    public function table(string $table): self;

    public function select(array $columns): self;

    public function distinct(): self;

    public function count(string $column): self;

    public function avg(string $column): self;

    public function sum(string $column): self;

    public function where(string $column, mixed $value, string $operator = '='): self;

    public function whereNot(string $column, mixed $value, string $operator = '='): self;

    public function andWhere(string $column, mixed $value, string $operator = '='): self;

    public function orWhere(string $column, mixed $value, string $operator = '='): self;

    public function whereIn(string $column, array $values): self;

    public function whereNotIn(string $column, array $values): self;

    public function whereBetween(string $column, mixed $start, mixed $end): self;

    public function whereNotBetween(string $column, mixed $start, mixed $end): self;

    public function whereIsNull(string $column): self;

    public function whereIsNotNull(string $column): self;

    public function and(): self;

    public function or(): self;

    public function like(string $column, string $value): self;

    public function notLike(string $column, string $value): self;

    public function having(string $column, mixed $value, string $operator = '='): self;

    public function orderBy(array|string $columns, array|string $order = ''): self;

    public function groupBy(string $column): self;

    public function limit(int $quantity, int $start = 0): self;

    public function find(int $id): Object;

    public function get(): array;

    public function first(): Object;

    public function insert(string $table, array $columns, array $values): void;

    public function update(string $table, array $data, int $id): void;

    public function delete(string $table, int $id): void;
}