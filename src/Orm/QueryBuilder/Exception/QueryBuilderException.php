<?php

declare(strict_types=1);

namespace Src\Orm\QueryBuilder\Exception;

class QueryBuilderException extends \Exception
{

}