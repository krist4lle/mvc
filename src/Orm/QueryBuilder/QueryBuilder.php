<?php

declare(strict_types=1);

namespace Src\Orm\QueryBuilder;

use Src\Base\BaseModel;
use Src\Orm\DataMapper\DataMapper;
use Src\Orm\QueryBuilder\Exception\QueryBuilderException;

class QueryBuilder implements QueryBuilderInterface
{
    private string $queryString = '';

    private array $fields = [
        'selectFunc' => '',
        'distinct' => '',
        'columns' => '*',
        'func' => null,
        'where' => null,
        'having' => null,
        'sort' => null,
        'limit' => null,
    ];

    private array $params = [];

    private DataMapper $mapper;

    public function __construct(string $model, string $table = null)
    {
        $this->mapper = new DataMapper($model);
        $this->table($table);
    }

    public function table(string $table): self
    {
        $this->fields['table'] = $table;

        return $this;
    }

    public function select(array $columns): self
    {
        $this->fields['columns'] = implode(', ', $columns);

        return $this;
    }

    public function distinct(): self
    {
        $this->fields['distinct'] = 'DISTINCT';

        return $this;
    }

    public function count(string $column): self
    {
        $this->fields['func'] = "COUNT({$column})";

        return $this;
    }

    public function avg(string $column): self
    {
        $this->fields['func'] = "AVG({$column})";

        return $this;
    }

    public function sum(string $column): self
    {
        $this->fields['func'] = "SUM({$column})";

        return $this;
    }

    public function where(string $column, mixed $value, string $operator = '='): self
    {
        $this->fields['where'] .= $this->getWhereString($column, $operator);
        $this->setParams($column, $value);

        return $this;
    }

    public function whereNot(string $column, mixed $value, string $operator = '='): self
    {
        $this->fields['where'] .= "NOT {$this->getWhereString($column, $operator)}";
        $this->setParams($column, $value);

        return $this;
    }

    public function andWhere(string $column, mixed $value, string $operator = '='): self
    {
        $this->fields['where'] .= "AND {$this->getWhereString($column, $operator)}";

        return $this;
    }

    public function orWhere(string $column, mixed $value, string $operator = '='): self
    {
        $this->fields['where'] .= "OR {$this->getWhereString($column, $operator)}";

        return $this;
    }

    public function whereIn(string $column, array $values): self
    {
        $this->fields['where'] .= $this->getWhereString($column, 'IN');
        $this->setParams($column, $values);

        return $this;
    }

    public function whereNotIn(string $column, array $values): self
    {
        $this->fields['where'] .= $this->getWhereString($column, 'NOT IN');
        $this->setParams($column, $values);

        return $this;
    }

    public function whereBetween(string $column, mixed $start, mixed $end): self
    {
        $values = "{$start} AND {$end}";
        $this->fields['where'] .= $this->getWhereString($column, 'BETWEEN');
        $this->setParams($column, $values);

        return $this;
    }

    public function whereNotBetween(string $column, mixed $start, mixed $end): self
    {
        $values = "{$start} AND {$end}";
        $this->fields['where'] .= $this->getWhereString($column, 'NOT BETWEEN');
        $this->setParams($column, $values);

        return $this;
    }

    public function whereIsNull(string $column): self
    {
        $this->fields['where'] .= "{$column} IS NULL";

        return $this;
    }

    public function whereIsNotNull(string $column): self
    {
        $this->fields['where'] .= "{$column} IS NOT NULL";

        return $this;
    }

    public function and(): self
    {
        $this->issetWhereClause();
        $this->fields['where'] .= "AND ";

        return $this;
    }

    public function or(): self
    {
        $this->issetWhereClause();
        $this->fields['where'] .= "OR ";

        return $this;
    }

    public function like(string $column, string $value): self
    {
        $this->fields['where'] .= $this->getWhereString($column, 'LIKE');
        $this->setParams($column, $value);

        return $this;
    }

    public function notLike(string $column, string $value): self
    {
        $this->fields['where'] .= $this->getWhereString($column, 'NOT LIKE');
        $this->setParams($column, $value);

        return $this;
    }

    public function having(string $column, mixed $value, string $operator = '='): self
    {
        $this->fields['having'] .= $this->getWhereString($column, $operator);
        $this->setParams($column, $value);

        return $this;
    }

    public function orderBy(array|string $columns, array|string $orders = ''): self
    {
        if (is_array($columns)) {
            foreach ($columns as $column) {
                if (is_array($orders)) {
                    $order = array_shift($orders);
                    $orderBy[$column] = $order;
                }
            }
            $this->fields['sort'] = 'ORDER BY ' . implode(', ', $orderBy);
        } else {
            $this->fields['sort'] = "ORDER BY {$columns} {$orders}";
        }

        return $this;
    }

    public function groupBy(string $column): self
    {
        $this->fields['sort'] = "GROUP BY {$column}";

        return $this;
    }

    public function limit(int $quantity, int $start = 0): self
    {
        $this->fields['limit'] = "LIMIT {$start}, {$quantity}";

        return $this;
    }

    public function find(int $id): object
    {
        return $this->where('id', $id)->first();
    }

    public function get(): array
    {
        try {
            $this->getQueryString();
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

        return $this->mapper->results();
    }

    public function first(): object
    {
        try {
            $this->getQueryString();
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

        return $this->mapper->result();
    }

    public function insert(string $table, array $columns, array $values): void
    {
        try {
            $columns = implode(', ', $columns);
            $values = implode("', '", $values);
            $this->queryString = "INSERT INTO {$table} ({$columns}) VALUES ('{$values}')";
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

    }

    public function update(string $table, array $data, int $id): void
    {
        try {
            $this->queryString = "UPDATE {$table} SET";
            $this->queryString .= " {$this->parseColumns(array_keys($data))} WHERE id = {$id}";
            foreach ($data as $column => $value) {
                $this->setParams($column, $value);
            }
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

    }

    public function delete(string $table, int $id): void
    {
        try {
            $this->queryString = "DELETE FROM {$table} WHERE id = {$id}";
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

    }

    public function showColumns()
    {
        try {
            $this->queryString = "SHOW COLUMNS FROM {$this->fields['table']}";
            $this->executeStatement();
        } catch (\PDOException $exception) {

            throw new QueryBuilderException($exception->getMessage(), (int)$exception->getCode());
        }

        return $this->mapper->results();
    }

    protected function getWhereString(string $column, string $operator): string
    {
        return "{$column} {$operator} (:{$column}) ";
    }

    protected function setParams(string $column, mixed $value): void
    {
        if (is_array($value)) {
            $value = implode("', '", $value);
        }
        $this->params[":{$column}"] = $value;
    }

    protected function getQueryString(): void
    {
        $this->queryString = "SELECT {$this->fields['distinct']}";
        if (isset($this->fields['func'])) {
            $this->queryString .= " {$this->fields['func']}";
        } else {
            $this->queryString .= " {$this->fields['columns']}";
        }
        $this->queryString .= " FROM {$this->fields['table']}";
        if (isset($this->fields['where'])) {
            $this->queryString .= " WHERE {$this->fields['where']}";
        }
        if (isset($this->fields['sort'])) {
            $this->queryString .= " {$this->fields['sort']}";
        }
        if (isset($this->fields['having'])) {
            $this->queryString .= " HAVING {$this->fields['having']}";
        }
        if (isset($this->fields['limit'])) {
            $this->queryString .= " {$this->fields['limit']}";
        }
    }

    protected function executeStatement()
    {
        $this->mapper->prepare($this->queryString);
        if (!empty($this->params)) {
            $this->mapper->bindParameters($this->params);
        }
        $this->mapper->execute();
        $this->resetData();
    }

    protected function issetWhereClause(): void
    {
        if (!isset($this->fields['where'])) {

            throw new QueryBuilderException('You have to use WHERE Clause first');
        }
    }

    protected function parseColumns(array $columns): string
    {
        for ($i = 0; $i < count($columns); $i++) {
            $columns[$i] = "{$columns[$i]} = :{$columns[$i]}";
        }

        return implode(', ', $columns);
    }

    protected function resetData(): void
    {
        $this->queryString = '';
        $this->params = [];
        $this->fields = [
            'selectFunc' => '',
            'distinct' => '',
            'columns' => '*',
            'func' => null,
            'where' => null,
            'having' => null,
            'sort' => null,
            'limit' => null,
        ];
    }
}