<?php

declare(strict_types=1);

namespace Src\Router;

interface RouterInterface
{
    public static function get(string $uri, array $action): void;

    public static function post(string $uri, array $action): void;

    public static function put(string $uri, array $action): void;

    public static function patch(string $uri, array $action): void;

    public static function delete(string $uri, array $action): void;
}