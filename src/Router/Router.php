<?php

declare(strict_types=1);

namespace Src\Router;

use Src\Base\BaseView;
use Src\Http\Request\ServerRequestInterface;

class Router implements RouterInterface
{
    private static ServerRequestInterface $request;

    public static mixed $apiResponse = '';

    public static mixed $webResponse = '';

    private static BaseView $view;

    public function __construct(ServerRequestInterface $request)
    {
        self::$request = $request;
        self::$view = new BaseView();
    }

    public static function get(string $uri, array $action): void
    {
        if (self::$request->getMethod() === "GET") {
            self::resolve($uri, $action);
        }
    }

    public static function post(string $uri, array $action): void
    {
        if (self::$request->getMethod() === "POST") {
            self::resolve($uri, $action);
        }
    }

    public static function put(string $uri, array $action): void
    {
        if (self::$request->getMethod() === "PUT") {
            self::resolve($uri, $action);
        }
    }

    public static function patch(string $uri, array $action): void
    {
        if (self::$request->getMethod() === "PATCH") {
            self::resolve($uri, $action);
        }
    }

    public static function delete(string $uri, array $action): void
    {
        if (self::$request->getMethod() === "DELETE") {
            self::resolve($uri, $action);
        }
    }

    private static function resolve(string $uri, array $action): void
    {
        if (self::$request->getUri()->getPath() === $uri) {
            $action[0] = new $action[0]();
            if (str_starts_with($uri, '/api')) {
                self::$apiResponse = call_user_func($action, self::$request);
            } else {
                self::$webResponse = call_user_func($action, self::$request);
            }
        } else {
            if (empty(self::$webResponse)){
                self::$webResponse = self::$view->getTemplate('404');
            }
        }
    }
}