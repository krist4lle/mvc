<?php

declare(strict_types=1);

namespace Src\Application;

use Src\Http\Request\ServerRequestFactory;
use Src\Http\Request\ServerRequestInterface;
use Src\Http\Response\Response;
use Src\Http\Response\ResponseSender;
use Src\Http\Stream\Stream;
use Src\Http\Stream\StreamInterface;
use Src\Router\Router;
use Src\Session\SessionManager;

class Application
{
    protected string $appRoot;

    public ServerRequestInterface $request;

    private mixed $apiResponse;

    private mixed $webResponse;

    public function __construct(string $appRoot)
    {
        $this->appRoot = $appRoot;
    }

    public function run(): self
    {
        $this->constants();
        if (version_compare($phpVersion = PHP_VERSION, $coreVersion = Config::MIN_VERSION, '<')) {
            die(sprintf('You are running PHP %s, but the core framework requires at least PHP %s,', $phpVersion, $coreVersion));
        }
        $this->enviroment();
        $this->errorHandler();

        return $this;
    }

    public function setSession(): self
    {
        (new SessionManager())->initialize();

        return $this;
    }

    public function request(array $server = null, array $query = null, array $body = null, array $cookies = null): self
    {
        $this->request = ServerRequestFactory::fromGlobals($server, $query, $body, $cookies);

        return $this;
    }

    public function routing(): self
    {
        require ROOT_DIR . '/routes/web.php';
        require ROOT_DIR . '/routes/api.php';

        $this->apiResponse = Router::$apiResponse;
        $this->webResponse = Router::$webResponse;

        return $this;
    }

    public function response(): self
    {
        if (!headers_sent()) {
            $response = new Response();
            $body = $this->getResponseBody('');
            if (!empty($this->webResponse)) {
                $body = $this->getResponseBody($this->webResponse);
                $response = $response->withHeader('content-type', 'text/html; charset=utf-8');
            }
            if (!empty($this->apiResponse)) {
                $body = $this->getResponseBody(json_encode($this->apiResponse));
                $response = $response->withHeader('content-type', 'application/json; charset=utf-8');
            }
            $response = $response->withBody($body);
            ResponseSender::send($response);
            exit();
        }

        return $this;
    }

    private function constants(): void
    {
        define('APP_ROOT', $this->appRoot);
        define('TEMPLATE_PATH', APP_ROOT . '/templates');
        define('LOG_DIR', APP_ROOT . '/tmp/log');
    }

    private function enviroment(): void
    {
        ini_set('default_charset', 'UTF-8');
    }

    private function errorHandler(): void
    {
        error_reporting(E_ALL | E_STRICT);
        set_error_handler('Src\ErrorHandling\ErrorHandling::errorHandler');
        set_exception_handler('Src\ErrorHandling\ErrorHandling::exceptionHandler');
    }

    private function getResponseBody(string $body): StreamInterface
    {
        $stream = new Stream('php://temp', 'wb+');
        $stream->write($body);
        $stream->rewind();

        return $stream;
    }
}