<?php

declare(strict_types=1);

namespace Src\Application;

class Config
{
    public const MIN_VERSION = '8.0';

    public const CORE_VERSION = '1.0.0';
}