<?php

declare(strict_types=1);

namespace Src\Base;

class BaseView
{
    public function getTemplate(string $template, array $params = []): string
    {
        ob_start();
        $content = $this->getContent($template, $params);
        require ROOT_DIR."/templates/layouts/app.php";

        return ob_get_clean();
    }

    private function getContent(string $template, $params = []): string
    {
        foreach ($params as $key => $value) {
            $$key = $value;
        }

        ob_start();
        require ROOT_DIR."/templates/{$template}.php";

        return ob_get_clean();
    }
}