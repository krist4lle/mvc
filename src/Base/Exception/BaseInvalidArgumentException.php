<?php

declare(strict_types=1);

namespace Src\Base\Exception;

class BaseInvalidArgumentException extends \InvalidArgumentException
{

}