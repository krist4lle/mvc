<?php

declare(strict_types=1);

namespace Src\Base;

class BaseController
{
    private object $view;

    public function __construct()
    {
        $this->view = new BaseView();
    }

    public function view(string $template, array $params = [])
    {
        return $this->view->getTemplate($template, $params);
    }
}