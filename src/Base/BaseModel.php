<?php

declare(strict_types=1);

namespace Src\Base;

use Src\Orm\Crud\Crud;

class BaseModel extends Crud
{
    public function __construct()
    {
        $this->model = $this->classBasename($this);
        $this->assignTableName();
        parent::__construct();
    }

    protected function assignTableName(string $tableName = null): void
    {
        $this->table = $tableName === null ? lcfirst($this->classBasename($this)).'s' : $tableName;
    }

    private function classBasename($class): string
    {
        $class = is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }
}