<?php

namespace Src\Database;

interface DatabaseInterface
{
    public function open(): \PDO;

    public function close(): void;
}