<?php

declare(strict_types=1);

namespace Src\Database;

class Database implements DatabaseInterface
{
    public ?\PDO $db;

    public function open(): \PDO
    {
        $config = require __DIR__ . '/../../config/database.php';

        try {
            $this->db = new \PDO(
                "mysql:host={$config['host']};port={$config['port']};dbname={$config['dbname']}",
                $config['username'],
                $config['password']
            );
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exception) {

            throw new DatabaseException($exception->getMessage(), (int) $exception->getCode());
        }

        return $this->db;
    }

    public function close(): void
    {
        $this->db = null;
    }
}