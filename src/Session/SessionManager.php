<?php

declare(strict_types=1);

namespace Src\Session;

use Src\Session\Storage\NativeSessionStorage;

class SessionManager
{
    public function initialize()
    {
        $factory = new SessionFactory();
        $options = require __DIR__.'/../../config/session.php';

        return $factory->create('', NativeSessionStorage::class, $options);
    }
}