<?php

declare(strict_types=1);

namespace Src\Session;

use Src\Session\Exception\SessionException;
use Src\Session\Exception\SessionStorageInvalidArgumentException;
use Src\Session\Storage\SessionStorageInterface;

class SessionFactory
{
    public function create(string $sessionName, string $storageString, array $options = []): SessionInterface
    {
        try {
            $storageObj = new $storageString($options);
            if (!$storageObj instanceof SessionStorageInterface) {

                throw new SessionStorageInvalidArgumentException($storageString.' is not a valid session storage object');
            }
            $session = new Session($sessionName, $storageObj);
        } catch (\Throwable $throwable) {

            throw new SessionException($throwable->getMessage());
        }

        return $session;
    }
}