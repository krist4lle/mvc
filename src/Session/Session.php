<?php

declare(strict_types=1);

namespace Src\Session;

use Src\Session\Exception\SessionException;
use Src\Session\Storage\SessionStorageInterface;

class Session implements SessionInterface
{
    protected SessionStorageInterface $storage;

    protected string $sessionName;

    protected const SESSION_PATTERN = '/^[a-zA-Z0-9_\.]{1,64}$/';

    public function __construct(string $sessionName, SessionStorageInterface $storage = null)
    {
        if (!$this->isSessionKeyValid($sessionName)) {

            throw new SessionException($sessionName.' is not a valid Session Name');
        }
        $this->sessionName = $sessionName;
        $this->storage = $storage;
    }

    public function set(string $key, mixed $value): void
    {
        try {
            $this->isSessionKeyValid($key);
            $this->storage->setSession($key, $value);
        } catch (\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    public function setArray(string $key, mixed $value): void
    {
        try {
            $this->isSessionKeyValid($key);
            $this->storage->setArraySession($key, $value);
        } catch (\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    public function get(string $key, mixed $default = null)
    {
        try {
            $this->isSessionKeyValid($key);

            return $this->storage->getSession($key, $default);

        } catch(\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    public function delete(string $key)
    {
        try {
            $this->isSessionKeyValid($key);

            return $this->storage->deleteSession($key);

        } catch (\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    public function invalidate(): void
    {
        $this->storage->invalidate();
    }

    public function flush(string $key, mixed $value = null)
    {
        try {
            $this->isSessionKeyValid($key);
            $this->storage->flush($key, $value);
        } catch(\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    public function has(string $key): bool
    {
        try {
            $this->isSessionKeyValid($key);

            return $this->storage->hasSession($key);

        } catch(\Throwable $throwable) {
            $this->errorMessage();
        }
    }

    protected function isSessionKeyValid(string $key): bool
    {
        if (preg_match(self::SESSION_PATTERN, $key) === false) {

            throw new SessionException($key.' is not a valid session key');
        }

        return true;
    }

    private function errorMessage(): \Throwable
    {
        throw new SessionException('An exception was thrown in retrieving the key from the session storage');
    }
}