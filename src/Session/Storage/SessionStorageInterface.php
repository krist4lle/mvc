<?php

declare(strict_types=1);

namespace Src\Session\Storage;

interface SessionStorageInterface
{
    public function setSessionName(string $sessionName): void;

    public function getSessionName(): string;

    public function setSessionId(string $sessionId): void;

    public function getSessionId(): string;

    public function setSession(string $key, mixed $value): void;

    public function setArraySession(string $key, mixed $value): void;

    public function getSession(string $key, mixed $default = null): mixed;

    public function deleteSession(string $key): void;

    public function invalidate(): void;

    public function flush(string $key, mixed $default = null): mixed;

    public function hasSession(string $key): bool;
}