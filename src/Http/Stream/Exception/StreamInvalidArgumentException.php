<?php

declare(strict_types=1);

namespace Src\Http\Stream\Exception;

class StreamInvalidArgumentException extends \InvalidArgumentException
{

}