<?php

declare(strict_types=1);

namespace Src\Http\Stream;

use Src\Http\Stream\Exception\StreamInvalidArgumentException;
use Src\Http\Stream\Exception\StreamRuntimeException;

class Stream implements StreamInterface
{
    protected $resource;

    protected $stream;

    public function __construct($stream, $mode = 'r')
    {
        $this->setStream($stream, $mode);
    }

    public function __toString()
    {
        if (!$this->isReadable()) {

            return '';
        }

        try {
            $this->rewind();
        } catch (StreamRuntimeException $e) {
            return '';
        }

        return $this->getContents();
    }

    public function close(): void
    {
        if (!$this->resource) {

            return;
        }
        $resource = $this->detach();
        fclose($resource);
    }

    public function detach()
    {
        $resource = $this->resource;
        $this->resource = null;

        return $resource;
    }

    public function attach($resource, $mode = 'r'): void
    {
        $this->setStream($resource, $mode);
    }

    public function getSize(): string|null
    {
        if ($this->resource === null) {

            return null;
        }
        $stats = fstat($this->resource);

        return $stats['size'];
    }

    public function tell(): int
    {
        if (!$this->resource) {
            throw new StreamRuntimeException('No resource available, cannot tell position');
        }
        $result = ftell($this->resource);
        if (!is_int($result)) {
            throw new StreamRuntimeException('Error occurred during tell operation');
        }

        return $result;
    }

    public function eof(): bool
    {
        if (!$this->resource) {

            return true;
        }

        return feof($this->resource);
    }

    public function isSeekable(): bool
    {
        if (!$this->resource) {

            return false;
        }
        $meta = stream_get_meta_data($this->resource);

        return $meta['seekable'];
    }

    public function seek($offset, $whence = SEEK_SET): bool
    {
        if (!$this->resource) {
            throw new StreamRuntimeException('No resource available, cannot seek position');
        }
        if (!$this->isSeekable()) {
            throw new StreamRuntimeException('Stream is not seekable');
        }
        $result = fseek($this->resource, $offset, $whence);

        if ($result !== 0) {
            throw new StreamRuntimeException('Error seeking within stream');
        }

        return true;
    }

    public function rewind(): bool
    {
        return $this->seek(0);
    }

    public function isWritable(): bool
    {
        if (!$this->resource) {

            return false;
        }

        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];

        return (
               strstr($mode, 'x')
            || strstr($mode, 'w')
            || strstr($mode, 'c')
            || strstr($mode, 'a')
            || strstr($mode, '+')
        );
    }

    public function write($string): int
    {
        if (!$this->resource) {
            throw new StreamRuntimeException('No resource available, cannot write');
        }
        if (!$this->isWritable()) {
            throw new StreamRuntimeException('Stream is not writable');
        }
        $result = fwrite($this->resource, $string);
        if ($result === false) {
            throw new StreamRuntimeException('Error writing to stream');
        }

        return $result;
    }

    public function isReadable(): bool
    {
        if (!$this->resource) {

            return false;
        }
        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];

        return (strstr($mode, 'r') || strstr($mode, '+'));
    }

    public function read(int $length): string
    {
        if (!$this->resource) {
            throw new StreamRuntimeException('No resource available, cannot read');
        }
        if (!$this->isReadable()) {
            throw new StreamRuntimeException('Stream is not readable');
        }
        $result = fread($this->resource, $length);
        if ($result === false) {
            throw new StreamRuntimeException('Error reading stream');
        }

        return $result;
    }

    public function getContents(): mixed
    {
        if (!$this->isReadable()) {
            throw new StreamRuntimeException('Stream is not readable');
        }
        $result = stream_get_contents($this->resource);
        if ($result === false) {
            throw new StreamRuntimeException('Error reading from stream');
        }

        return $result;
    }

    public function getMetadata($key = null): array|null
    {
        if ($key === null) {
            return stream_get_meta_data($this->resource);
        }
        $metadata = stream_get_meta_data($this->resource);
        if (!array_key_exists($key, $metadata)) {

            return null;
        }

        return $metadata[$key];
    }

    private function setStream($stream, $mode = 'r')
    {
        $resource = $stream;
        if (is_string($stream)) {
            $resource = fopen($stream, $mode);
        }
        if (!is_resource($resource) || 'stream' !== get_resource_type($resource)) {
            throw new StreamInvalidArgumentException(
                'Invalid stream provided, must be a string stream identifier or stream resource'
            );
        }
        if ($stream !== $resource) {
            $this->stream = $stream;
        }

        $this->resource = $resource;
    }
}