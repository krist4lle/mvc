<?php

declare(strict_types=1);

namespace Src\Http\Response;

use Src\Http\Message\MessageTrait;
use Src\Http\Response\Exception\ResponseInvalidArgumentException;
use Src\Http\Stream\Stream;
use Src\Http\Stream\StreamInterface;

class Response implements ResponseInterface
{
   use MessageTrait;

    private $phrases = [
        // INFORMATIONAL CODES
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        // SUCCESS CODES
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        // REDIRECTION CODES
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        // CLIENT ERROR
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        // SERVER ERROR
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    ];

    private string $reasonPhrase = '';

    private int $statusCode;

    public function __construct($body = 'php://memory', int $status = 200, array $headers = [])
    {
        if (!is_string($body) && !is_resource($body) && !$body instanceof StreamInterface) {
            throw new ResponseInvalidArgumentException(
                'Stream must be a string stream resource identifier, '
                . 'an actual stream resource, or a StreamInterface implementation'
            );
        }
        $this->setStatusCode($status);
        $this->stream = ($body instanceof StreamInterface) ? $body : new Stream($body, 'wb+');
        $this->headers = $headers;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getReasonPhrase(): string
    {
        if (!$this->reasonPhrase && isset($this->phrases[$this->statusCode])) {
            $this->reasonPhrase = $this->phrases[$this->statusCode];
        }

        return $this->reasonPhrase;
    }

    public function withStatus(int $code, string $reasonPhrase = ''): self
    {
        $new = clone $this;
        $new->setStatusCode($code);
        $new->reasonPhrase = $reasonPhrase;

        return $new;
    }

    private function setStatusCode(mixed $code): void
    {
        if (!is_numeric($code) || is_float($code) || $code < 100 || $code >= 600
        ) {
            throw new ResponseInvalidArgumentException(
                'Invalid status code, must be an integer between 100 and 599, inclusive'
            );
        }

        $this->statusCode = $code;
    }
}