<?php

declare(strict_types=1);

namespace Src\Http\Response;

use Src\Http\Response\Exception\ResponseRuntimeException;

class ResponseSender
{
    public static function send(ResponseInterface $response): void
    {
        if (headers_sent()) {
            throw new ResponseRuntimeException('Unable to send response, headers already sent');
        }

        header(sprintf(
            'HTTP/%s %d %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ));
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        echo $response->getBody()->getContents();
    }
}