<?php

declare(strict_types=1);

namespace Src\Http\Response\Exception;

class ResponseRuntimeException extends \RuntimeException
{

}