<?php

declare(strict_types=1);

namespace Src\Http\Response;

use Src\Base\BaseModel;

abstract class ApiResponse
{
    public abstract function requiredData(): array;

    public function response(BaseModel $model): \stdClass
    {
        $object = new \stdClass();
        foreach ($this->requiredData() as $field)
        {
            $object->$field = $model->$field;
        }

        return $object;
    }

    public function collection(array $collection): array
    {
        foreach ($collection as $i => $object) {
            $collection[$i] = $this->response($object);
        }

        return $collection;
    }
}