<?php

declare(strict_types=1);

namespace Src\Http\Uri;

use Src\Http\Uri\Exception\UriInvalidArgumentException;

class Uri implements UriInterface
{
    private ?string $uri;
    private string $scheme = '';
    private string $userInfo = '';
    private string $host = '';
    private int $port;
    private string $path = '';
    private string $query = '';
    private string $fragment = '';

    protected $allowedSchemes = [
        'http' => 80,
        'https' => 443,
    ];

    public function __construct(string $uri = '')
    {
        if (!is_string($uri)) {
            throw new UriInvalidArgumentException('URI passed to constructor must be a string');
        }
        if (!empty($uri)) {
            $this->parseUri($uri);
        }
    }

    public function __clone()
    {
        $this->uri = null;
    }

    public function __toString()
    {
        if ($this->uri === null) {
            $this->uri = static::createUriString(
                $this->scheme,
                $this->getAuthority(),
                $this->getPath(),
                $this->query,
                $this->fragment
            );
        }

        return $this->uri;
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getAuthority(): string
    {
        if (!empty($this->host)) {
            $authority = $this->host;
            if (!empty($this->userInfo)) {
                $authority = $this->userInfo . '@' . $authority;
            }
            if (!empty($this->port)) {
                $authority .= ':' . $this->port;
            }

            return $authority;
        }

        return '';
    }

    public function getUserInfo(): string
    {
        return $this->userInfo;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): int|null
    {
        return $this->port ?? null;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getFragment(): string
    {
        return $this->fragment;
    }

    public function withScheme(mixed $scheme): self
    {
        if (!is_string($scheme)) {
            throw new UriInvalidArgumentException('Expects a string argument');
        }

        $scheme = $this->filterScheme($scheme);
        $new = clone $this;
        if ($scheme !== $this->scheme) {
            $new->scheme = $scheme;
        }

        return $new;
    }

    public function withUserInfo(mixed $user, mixed $password = null): self
    {
        if (!is_string($user)) {
            throw new UriInvalidArgumentException('Expects a string user argument');
        }
        if ($password !== null && !is_string($password)) {
            throw new UriInvalidArgumentException('Expects a string password argument');
        }
        if (isset($password)) {
            $user .= ':' . $password;
        }
        $new = clone $this;
        if ($user !== $this->userInfo) {
            $new->userInfo = $user;
        }

        return $new;
    }

    public function withHost(mixed $host): self
    {
        if (!is_string($host)) {
            throw new UriInvalidArgumentException('Expects a string argument',);
        }
        $new = clone $this;
        if ($host !== $this->host) {
            $new->host = $host;
        }

        return $new;
    }

    public function withPort(int|null $port): self
    {
        if ($port !== null) {
            if (!in_array($port, range(32768, 65535)) || !in_array($port, range(4096, 65535))) {
                throw new UriInvalidArgumentException('Port value must be in TCP or UDP range');
            }
        }
        $new = clone $this;
        if ($port !== $this->port) {
            $new->port = $port;
        }

        return $new;
    }

    public function withPath(string $path): self
    {
        if (str_contains($path, '?')) {
            throw new UriInvalidArgumentException('Invalid path provided, must not contain a query string');
        }
        if (str_contains($path, '#')) {
            throw new UriInvalidArgumentException('Invalid path provided; must not contain a URI fragment');
        }
        $new = clone $this;
        if ($path !== $this->path) {
            $new->path = $path;
        }

        return $new;
    }

    public function withQuery(mixed $query): self
    {
        if (!is_string($query)) {
            throw new UriInvalidArgumentException('Query string must be a string');
        }
        if (str_contains($query, '#')) {
            throw new UriInvalidArgumentException('Query string must not include a URI fragment');
        }
        $query = $this->filterQuery($query);
        $new = clone $this;
        if ($query !== $this->query) {
            $new->query = $query;
        }

        return $new;
    }

    public function withFragment(mixed $fragment): self
    {
        if (!is_string($fragment)) {
            throw new UriInvalidArgumentException('Expects a string argument');
        }
        $fragment = $this->filterFragment($fragment);
        $new = clone $this;
        if ($fragment !== $this->fragment) {
            $new->fragment = $fragment;
        }

        return $new;
    }

    private function parseUri(string $uri)
    {
        $parts = parse_url($uri);
        if ($parts !== false) {
            $this->scheme = isset($parts['scheme']) ? $this->filterScheme($parts['scheme']) : '';
            $this->userInfo = $parts['user'] ?? '';
            if (isset($parts['pass'])) {
                $this->userInfo .= ':' . $parts['pass'];
            }
            $this->host = $parts['host'] ?? '';
            $this->port = $parts['port'] ?? null;
            $this->path = $parts['path'] ?? '';
            $this->query = isset($parts['query']) ? $this->filterQuery($parts['query']) : '';
            $this->fragment = isset($parts['fragment']) ? $this->filterFragment($parts['fragment']) : '';
        }

        throw new UriInvalidArgumentException('The source URI string appears to be malformed');
    }

    private function filterScheme(string $scheme): string
    {
        $scheme = strtolower($scheme);
        $scheme = preg_replace('#:(//)?$#', '', $scheme);
        if (empty($scheme)) {

            return '';
        }
        if (!array_key_exists($scheme, $this->allowedSchemes)) {
            throw new UriInvalidArgumentException('Unsupported scheme must be any empty string, http or https');
        }

        return $scheme;
    }

    private function filterQuery(string $query): string
    {
        $query = $this->parseString($query);
        if (!empty($query)) {
            $parts = explode('&', $query);

            foreach ($parts as $i => $part) {
                list($key, $value) = $this->splitQueryValue($part);
                $parts[$i] = sprintf('%s=%s', $key, $value);
            }

            return implode('&', $parts);
        }

        return $query;
    }

    private function filterFragment(string $fragment): string
    {
        return $this->parseString($fragment);
    }

    private function splitQueryValue(string $value): array
    {
        $data = explode('=', $value, 2);
        if (count($data) !== 2) {

            throw new UriInvalidArgumentException('Invalid query string provided');
        }

        return $data;
    }

    private function parseString(string $value): string
    {
        if (!empty($value) && str_starts_with($value, '#')) {

            return substr($value, 1);
        }

        return $value;
    }

    private static function createUriString($scheme, $authority, $path, $query, $fragment): string
    {
        $uri = '';
        if (!empty($scheme)) {
            $uri .= sprintf('%s://', $scheme);
        }
        if (!empty($authority)) {
            $uri .= $authority;
        }
        if (isset($path)) {
            if (empty($path) || !str_starts_with($path, '/')) {
                $path = '/' . $path;
            }
            $uri .= $path;
        }
        if (isset($query)) {
            $uri .= sprintf('?%s', $query);
        }
        if (isset($fragment)) {
            $uri .= sprintf('#%s', $fragment);
        }

        return $uri;
    }
}