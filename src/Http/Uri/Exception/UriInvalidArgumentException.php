<?php

declare(strict_types=1);

namespace Src\Http\Uri\Exception;

class UriInvalidArgumentException extends \InvalidArgumentException
{

}