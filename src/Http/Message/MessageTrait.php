<?php

declare(strict_types=1);

namespace Src\Http\Message;

use InvalidArgumentException;
use Src\Http\Message\Exception\MessageInvalidArgumentException;
use Src\Http\Stream\StreamInterface;

trait MessageTrait
{
    protected array $headers = [];

    protected array $headerNames = [];

    private string $protocol = '1.1';

    private StreamInterface $stream;

    public function getProtocolVersion(): string
    {
        return $this->protocol;
    }

    public function withProtocolVersion($version): self
    {
        if ($version !== '1.0' | '1.1' | '2' | '3') {

            throw new MessageInvalidArgumentException(
                'The version string MUST contain only the HTTP version number (e.g., "1.1", "1.0").'
            );
        }
        $new = clone $this;
        $new->protocol = $version;

        return $new;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function hasHeader($header): bool
    {
        return array_key_exists(strtolower($header), $this->headerNames);
    }

    public function getHeader($header): array|string
    {
        if (!$this->hasHeader($header)) {
            return [];
        }

        $header = $this->headerNames[strtolower($header)];
        $value  = $this->headers[$header];
        $value  = is_array($value) ? $value : [$value];

        return $value;
    }

    public function getHeaderLine($name): string
    {
        $value = $this->getHeader($name);
        if (empty($value)) {
            return '';
        }

        return implode(',', $value);
    }

    public function withHeader($header, $value): self
    {
        $value = $this->validHeaderValue($value);
        $normalized = strtolower($header);
        $this->validHeaderName($normalized);
        $new = clone $this;
        if ($new->hasHeader($header)) {
            unset($new->headers[$new->headerNames[$normalized]]);
        }
        $new->headerNames[$normalized] = $header;
        $new->headers[$header] = $value;

        return $new;
    }

    public function withAddedHeader($header, $value): self
    {
        $value = $this->validHeaderValue($value);
        if (!$this->hasHeader($header)) {

            return $this->withHeader($header, $value);
        }
        $normalized = strtolower($header);
        $header = $this->headerNames[$normalized];
        $new = clone $this;
        $new->headers[$header] = array_merge($this->headers[$header], $value);

        return $new;
    }

    public function withoutHeader($header): self
    {
        if (!$this->hasHeader($header)) {

            return clone $this;
        }
        $normalized = strtolower($header);
        $original = $this->headerNames[$normalized];
        $new = clone $this;
        unset($new->headers[$original], $new->headerNames[$normalized]);

        return $new;
    }

    public function getBody(): StreamInterface
    {
        return $this->stream;
    }

    public function withBody(StreamInterface $body): self
    {
        $new = clone $this;
        $new->stream = $body;

        return $new;
    }

    private function arrayContainsOnlyStrings(array $array): bool
    {
        return array_reduce($array, [__CLASS__, 'filterStringValue'], true);
    }

    private static function filterStringValue(mixed $carry, mixed $item): bool
    {
        if (!is_string($item)) {

            return false;
        }

        return $carry;
    }

    private function validHeaderName(string $header): void
    {
        $validHeaderNames = [
            'a-im',
            'accept',
            'accept-charset',
            'accept-encoding',
            'accept-language',
            'accept-datetime',
            'access-control-request-method',
            'access-control-request-headers',
            'authorization',
            'cache-control',
            'connection',
            'content-length',
            'content-type',
            'cookie',
            'date',
            'expect',
            'forwarded',
            'from',
            'host',
            'if-match',
            'if-modified-since',
            'if-none-match',
            'if-range',
            'if-unmodified-since',
            'max-forwards',
            'origin',
            'pragma',
            'proxy-authorization',
            'range',
            'referer',
            'te',
            'user-agent',
            'upgrade',
            'via',
            'warning',
            'dnt',
            'x-requested-with',
            'x-csrf-token'
        ];
        if (!in_array($header, $validHeaderNames, true)) {
            throw new MessageInvalidArgumentException(
                'Please provide a valid HTTP header name.'
            );
        }
    }

    private function validHeaderValue(mixed $value): array
    {
        if (is_string($value)) {
            $value = [ $value ];
        }
        if (!is_array($value) || !$this->arrayContainsOnlyStrings($value)) {
            throw new InvalidArgumentException(
                'Invalid header value; must be a string or array of strings'
            );
        }

        return $value;
    }
}
