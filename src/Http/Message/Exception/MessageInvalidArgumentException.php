<?php

declare(strict_types=1);

namespace Src\Http\Message\Exception;

class MessageInvalidArgumentException extends \InvalidArgumentException
{

}