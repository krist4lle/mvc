<?php

declare(strict_types=1);

namespace Src\Http\Request;


use Src\Http\Request\Exception\RequestInvalidArgumentException;
use Src\Http\Stream\Stream;
use Src\Http\Stream\StreamInterface;
use Src\Http\Uri\Uri;
use Src\Http\Uri\UriInterface;

trait RequestTrait
{
    private string $method = '';

    private ?string $requestTarget;

    private ?UriInterface $uri;

    private function initialize($uri = null, $method = null, $body = 'php://memory', array $headers = []): void
    {
        $this->validateUri($uri);
        $this->validateMethod($method);
        $this->validateBody($body);
        if (is_string($uri)) {
            $uri = new Uri($uri);
        }

        $this->method = $method ?: '';
        $this->uri    = $uri ?: new Uri();
        $this->stream = ($body instanceof StreamInterface) ? $body : new Stream($body, 'w+');
        $this->headers = $headers;
    }

    public function getRequestTarget(): string
    {
        if ($this->requestTarget !== null) {
            return $this->requestTarget;
        }
        if (!$this->uri) {
            return '/';
        }

        $target = $this->uri->getPath();
        if ($this->uri->getQuery()) {
            $target .= '?' . $this->uri->getQuery();
        }

        if (empty($target)) {
            $target = '/';
        }

        return $target;
    }

    public function withRequestTarget(mixed $requestTarget): self
    {
        if (preg_match('#\s#', $requestTarget)) {
            throw new RequestInvalidArgumentException('Invalid request target provided, cannot contain whitespace');
        }
        $new = clone $this;
        $new->requestTarget = $requestTarget;

        return $new;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function withMethod(mixed $method): self
    {
        $this->validateMethod($method);
        $new = clone $this;
        $new->method = $method;

        return $new;
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    public function withUri(UriInterface $uri, $preserveHost = false): self
    {
        $new = clone $this;
        $new->uri = $uri;
        if ($preserveHost && $this->hasHeader('Host')) {
            return $new;
        }
        if (!$uri->getHost()) {
            return $new;
        }
        $host = $uri->getHost();
        if ($uri->getPort()) {
            $host .= ':' . $uri->getPort();
        }
        $new->headerNames['host'] = 'Host';

        foreach (array_keys($new->headers) as $header) {
            if (strtolower($header) === 'host') {
                unset($new->headers[$header]);
            }
        }
        $new->headers['Host'] = [$host];

        return $new;
    }

    private function validateUri(mixed $uri): void
    {
        if (!$uri instanceof UriInterface && !is_string($uri) && $uri !== null) {
            throw new RequestInvalidArgumentException(
                'Invalid URI provided: must be null, a string, or a UriInterface instance'
            );
        }
    }

    private function validateMethod(mixed $method):void
    {
        $legalMethods = ['GET', 'HEAD', 'POST', 'PATCH', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE'];
        if ($method !== null) {
            if (is_string($method)) {
                foreach ($legalMethods as $legalMethod) {
                    if ($legalMethod === $method) {

                        return;
                    }
                }
                throw new RequestInvalidArgumentException('Unsupported HTTP method');
            }
            throw new RequestInvalidArgumentException('HTTP method must be a string');
        }
    }

    private function validateBody(mixed $body): void
    {
        if (!is_string($body) && !is_resource($body) && !$body instanceof StreamInterface) {
            throw new RequestInvalidArgumentException(
                'Body must be a string stream resource identifier, an actual stream resource, or a StreamInterface implementation'
            );
        }
    }

    private function getHostFromUri(): string
    {
        $host  = $this->uri->getHost();
        $host .= $this->uri->getPort() ? ':' . $this->uri->getPort() : '';

        return $host;
    }
}
