<?php

declare(strict_types=1);

namespace Src\Http\Request;

use Src\Http\Message\MessageTrait;

class Request implements RequestInterface
{
    use MessageTrait, RequestTrait;

    public function __construct($uri = null, $method = null, $body = 'php://temp', array $headers = [])
    {
        $this->initialize($uri, $method, $body, $headers);
    }

    public function getHeaders(): array
    {
        $headers = $this->headers;
        if (!$this->hasHeader('host') && ($this->uri && $this->uri->getHost())) {
            $headers['Host'] = [$this->getHostFromUri()];
        }

        return $headers;
    }

    public function getHeader(string $header): array
    {
        if (!$this->hasHeader($header)) {
            if (strtolower($header) === 'host' && ($this->uri && $this->uri->getHost())) {

                return [$this->getHostFromUri()];
            }

            return [];
        }

        $header = $this->headerNames[strtolower($header)];
        $value = $this->headers[$header];

        return is_array($value) ? $value : [$value];
    }
}