<?php

declare(strict_types=1);

namespace Src\Http\Request;

use Src\Http\Request\Validator\Exception\ValidationInvalidArgumentException;
use Src\Http\Request\Validator\Validator;

abstract class FormRequest extends ServerRequest
{
    private $validator;

    public function __construct()
    {
        parent::__construct();
        $this->validator = new Validator();
    }

    abstract public function rules(): array;

    public function validate(array $rules): array
    {
        if (empty($rules)) {
            throw new ValidationInvalidArgumentException('Provide appropriate rules to this method');
        }

        return  $this->validator->validate($this->getData(), $rules);
    }

    public function validated(): array
    {
        return $this->validator->validated($this->getData(), $this->rules());
    }

    private function getData(): array
    {
        $json = $this->getBody()->getContents();

        return json_decode($json, true);
    }
}