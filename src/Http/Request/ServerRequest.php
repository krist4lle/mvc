<?php

declare(strict_types=1);

namespace Src\Http\Request;

use Src\Http\Message\MessageTrait;
use Src\Http\Stream\Stream;
use Src\Http\Stream\StreamInterface;

class ServerRequest implements ServerRequestInterface
{
    use MessageTrait, RequestTrait;

    private $attributes = [];

    private $cookieParams = [];

    private $parsedBody;

    private $queryParams = [];

    private $serverParams;

    public function __construct(
        array $serverParams = [],
              $uri = null,
              $method = null,
              $body = 'php://input',
        array $headers = [],
        array $cookies = [],
        array $queryParams = [],
              $parsedBody = null,
              $protocol = '1.1'
    ) {
        $body = $this->getStream($body);
        $this->initialize($uri, $method, $body, $headers);
        $this->serverParams = $serverParams;
        $this->cookieParams = $cookies;
        $this->queryParams = $queryParams;
        $this->parsedBody = $parsedBody;
        $this->protocol  = $protocol;
    }

    public function getServerParams(): array
    {
        return $this->serverParams;
    }

    public function getUploadedFiles()
    {
        //
    }

    public function withUploadedFiles($uploadedFiles)
    {
        //
    }

    public function getCookieParams(): array
    {
        return $this->cookieParams;
    }

    public function withCookieParams(array $cookies): self
    {
        $new = clone $this;
        $new->cookieParams = $cookies;

        return $new;
    }

    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function withQueryParams(array $query): self
    {
        $new = clone $this;
        $new->queryParams = $query;

        return $new;
    }

    public function getParsedBody(): array
    {
        return $this->parsedBody;
    }

    public function withParsedBody(array|null|object $data): self
    {
        $new = clone $this;
        $new->parsedBody = $data;

        return $new;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getAttribute(string $attribute, mixed $default = null)
    {
        if (!array_key_exists($attribute, $this->attributes)) {

            return $default;
        }

        return $this->attributes[$attribute];
    }

    public function withAttribute(string $attribute, mixed $value): self
    {
        $new = clone $this;
        $new->attributes[$attribute] = $value;

        return $new;
    }

    public function withoutAttribute(string $attribute): self
    {
        $new = clone $this;
        if (isset($this->attributes[$attribute])) {
            unset($new->attributes[$attribute]);
        }

        return $new;
    }

    public function getMethod()
    {
        if (empty($this->method)) {

            return 'GET';
        }

        return $this->method;
    }

    public function withMethod(string $method): self
    {
        $this->validateMethod($method);
        $new = clone $this;
        $new->method = $method;

        return $new;
    }

    private function getStream(mixed $stream): StreamInterface
    {
        return $stream instanceof StreamInterface ? $stream : new Stream($stream, 'r');
    }
}