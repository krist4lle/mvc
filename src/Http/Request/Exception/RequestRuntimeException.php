<?php

declare(strict_types=1);

namespace Src\Http\Request\Exception;

class RequestRuntimeException extends \RuntimeException
{

}