<?php

declare(strict_types=1);

namespace Src\Http\Request;

use Src\Http\Uri\Uri;
use Src\Http\Uri\UriInterface;

abstract class ServerRequestFactory
{
    private static $apacheRequestHeaders = 'apache_request_headers';


    public static function fromGlobals(array $server = null, array $query = null, array $body = null, array $cookies = null): ServerRequestInterface
    {
        $server = static::normalizeServer($server ?: $_SERVER);
        $headers = static::marshalHeaders($server);

        return new ServerRequest(
            $server,
            static::marshalUriFromServer($server, $headers),
            static::getValue('REQUEST_METHOD', $server, 'GET'),
            'php://input',
            $headers,
            $cookies ?: $_COOKIE,
            $query ?: $_GET,
            $body ?: $_POST,
            static::getValue('SERVER_PROTOCOL', $server, '1.1'),
        );
    }

    public static function normalizeServer(array $server): array
    {
        $apacheRequestHeaders = self::$apacheRequestHeaders;
        if (isset($server['HTTP_AUTHORIZATION']) || !is_callable($apacheRequestHeaders)) {


            return $server;
        }

        $apacheRequestHeaders = $apacheRequestHeaders();
        if (isset($apacheRequestHeaders['Authorization'])) {
            $server['HTTP_AUTHORIZATION'] = $apacheRequestHeaders['Authorization'];

            return $server;
        }

        if (isset($apacheRequestHeaders['authorization'])) {
            $server['HTTP_AUTHORIZATION'] = $apacheRequestHeaders['authorization'];

            return $server;
        }

        return $server;
    }

    public static function marshalHeaders(array $server)
    {
        $headers = [];
        foreach ($server as $key => $value) {
            if ($value && str_starts_with($key, 'HTTP_')) {
                $name = strtr(substr($key, 5), '_', ' ');
                $name = strtr(ucwords(strtolower($name)), ' ', '-');
                $name = strtolower($name);
                $headers[$name] = $value;

                continue;
            }

            if ($value && str_starts_with($key, 'CONTENT_')) {
                $name = substr($key, 8); // Content-
                $name = 'Content-' . (($name == 'MD5') ? $name : ucfirst(strtolower($name)));
                $name = strtolower($name);
                $headers[$name] = $value;
            }
        }

        return $headers;
    }

    public static function marshalUriFromServer(array $server, array $headers): UriInterface
    {
        $uri = new Uri('');
        $scheme = 'http';
        $https = self::getValue('HTTPS', $server);
        if (($https && $https !== 'off') || self::getHeader('x-forwarded-proto', $headers, false) === 'https') {
            $scheme = 'https';
        }
        $uri = $uri->withScheme($scheme);

        $accumulator = (object)['host' => '', 'port' => null];
        self::marshalHostAndPortFromHeaders($accumulator, $server, $headers);
        $host = $accumulator->host;
        $port = $accumulator->port;
        if (!empty($host)) {
            $uri = $uri->withHost($host);
            if (!empty($port)) {
                $uri = $uri->withPort($port);
            }
        }

        $path = self::marshalRequestUri($server);
        $path = self::stripQueryString($path);

        $query = isset($server['QUERY_STRING']) ? ltrim($server['QUERY_STRING'], '?') : '';

        return $uri->withPath($path)->withQuery($query);
    }

    public static function getHeader(string $header, array $headers, mixed $default = null): mixed
    {
        $header = strtolower($header);
        $headers = array_change_key_case($headers, CASE_LOWER);
        if (array_key_exists($header, $headers)) {

            return is_array($headers[$header]) ? implode(', ', $headers[$header]) : $headers[$header];
        }

        return $default;
    }

    public static function marshalHostAndPortFromHeaders($accumulator, array $server, array $headers): void
    {
        if (!self::getHeader('host', $headers, false)) {
            if (isset($server['SERVER_NAME'])) {
                $accumulator->host = $server['SERVER_NAME'];
                if (isset($server['SERVER_PORT'])) {
                    $accumulator->port = (int)$server['SERVER_PORT'];
                }
            }
        }
        self::marshalHostAndPortFromHeader($accumulator, self::getHeader('host', $headers));
    }

    public static function marshalRequestUri(array $server): string
    {
        $iisUrlRewritten = self::getValue('IIS_WasUrlRewritten', $server);
        $unencodedUrl = self::getValue('UNENCODED_URL', $server, '');
        if ($iisUrlRewritten == '1' && !empty($unencodedUrl)) {

            return $unencodedUrl;
        }

        $requestUri = self::getValue('REQUEST_URI', $server);
        $httpXRewriteUrl = self::getValue('HTTP_X_REWRITE_URL', $server);
        if ($httpXRewriteUrl !== null) {
            $requestUri = $httpXRewriteUrl;
        }
        $httpXOriginalUrl = self::getValue('HTTP_X_ORIGINAL_URL', $server);
        if ($httpXOriginalUrl !== null) {
            $requestUri = $httpXOriginalUrl;
        }
        if ($requestUri !== null) {

            return preg_replace('#^[^/:]+://[^/]+#', '', $requestUri);
        }
        $origPathInfo = self::getValue('ORIG_PATH_INFO', $server);

        return empty($origPathInfo) ? '/' : $origPathInfo;
    }

    public static function stripQueryString(string $path): string
    {
        $qpos = strpos($path, '?');

        return $qpos !== false ? substr($path, 0, $qpos) : $path;
    }

    private static function marshalHostAndPortFromHeader($accumulator, mixed $host): void
    {
        if (is_array($host)) {
            $host = implode(', ', $host);
        }
        $accumulator->host = $host;
        $accumulator->port = null;
    }

    public static function getValue(string $key, array $values, string $default = null): null|string
    {
        return array_key_exists($key, $values) ? $values[$key] : $default;
    }
}
