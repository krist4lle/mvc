<?php

declare(strict_types=1);

namespace Src\Http\Request\Validator;

interface ValidatorInterface
{
    public function validate(array $data, array $validationRules): array;

    /**
     * Get the attributes and values that were validated.
     */
    public function validated(array $data, array $validationRules): array;

    /**
     * Determine if the data fails the validation rules.
     *
     * @return bool
     */
    public function fails(array $data, array $validationRules): bool;

    /**
     * Get the failed validation rules.
     *
     * @return array
     */
    public function failed(): array;

    /**
     * Get all of the validation error messages.
     */
    public function errors(array $failed): array;
}