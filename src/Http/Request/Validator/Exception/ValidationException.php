<?php

declare(strict_types=1);

namespace Src\Http\Request\Validator\Exception;

class ValidationException extends \Exception
{

}