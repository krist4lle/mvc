<?php

declare(strict_types=1);

namespace Src\Http\Request\Validator;

trait RulesTrait
{
    protected function required(mixed $value): bool
    {
        return (isset($value) && $value !== '');
    }

    protected function string(mixed $value): bool
    {
        return is_string($value);
    }

    protected function number(mixed $value): bool
    {
        return (is_int($value) || is_float($value));
    }

    protected function alpha(string $value): bool
    {
        $restrictedSymbols = ['!', '&', '*', ';', '+', '{', '}', '[', ']', "'", '/', '#', '$', '^'];
        foreach ($restrictedSymbols as $symbol) {
            if (strpos($value, $symbol)) {

                return false;
            }
        }

        return true;
    }

    protected function exists(mixed $value, array $arguments = []): bool
    {
        $data = $this->findExistingData($value, $arguments);

        return !empty($data);
    }

    protected function unique(mixed $value, array $arguments = []): bool
    {
        $data = $this->findExistingData($value, $arguments);

        return empty($data);
    }

    private function findExistingData(mixed $value, array $arguments): array
    {
        $arguments = explode(',', array_shift($arguments));
        $table = array_shift($arguments);
        $column = array_shift($arguments);

        return empty($value) ? [] : $this->model->query()->table($table)->where($column, $value)->get();
    }
}