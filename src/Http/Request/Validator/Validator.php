<?php

declare(strict_types=1);

namespace Src\Http\Request\Validator;

use Src\Base\BaseModel;
use Src\Http\Response\Response;
use Src\Http\Response\ResponseSender;
use Src\Http\Stream\Stream;
use Src\Http\Stream\StreamInterface;

class Validator implements ValidatorInterface
{
    use RulesTrait;

    protected BaseModel $model;

    public function __construct()
    {
        $this->model = new BaseModel();
    }

    public function validate(array $data, array $validationRules): array
    {
        if ($this->fails($data, $validationRules)) {
            $messages = $this->errors($this->failed());
            $this->response(new Response(), $messages);
        }

        return $data;
    }

    public function validated(array $data, array $validationRules): array
    {
        return $this->validate($data, $validationRules);
    }

    public function fails(array $data, array $validationRules): bool
    {
        foreach ($validationRules as $field => $rules) {
            foreach (explode('|', $rules) as $rule) {
                $arguments[$field] = [];
                if (strpos($rule, '=')) {
                    parse_str($rule, $arguments);
                    $rule = key($arguments);
                }
                $outcomes[$rule] = call_user_func_array(array($this, $rule), array($data[$field], $arguments));
            }
            $results[$field] = $outcomes;
            unset($outcomes);
            $fails[$field] = in_array(false, $results[$field], true);
        }
        $this->results = $results;

        return in_array(true, $fails, true);
    }

    public function failed(): array
    {
        foreach ($this->results as $field => $results) {
            unset($this->results[$field]);
            if (in_array(false, $results, true)) {
                $this->results[$field] = array_keys($results, false, true);
            }
        }

        return $this->results;
    }

    public function errors(array $failed): array
    {
        foreach ($failed as $field => $rules) {
            foreach ($rules as $rule) {
                switch ($rule) {
                    case 'required':
                        $messages[$field][$rule] = "Please submit '{$field}' field";
                        break;
                    case 'string':
                        $messages[$field][$rule] = "Provided '{$field}' field must be a string type";
                        break;
                    case 'number':
                        $messages[$field][$rule] = "Provided '{$field}' field must be a number type";
                        break;
                    case 'alpha':
                        $messages[$field][$rule] = "Provided '{$field}' field must not contain any restricted symbols";
                        break;
                    case 'exists':
                        $messages[$field][$rule] = "Please provide an existing value for '{$field}' field";
                        break;
                    case 'unique':
                        $messages[$field][$rule] = "Please provide an unique value for '{$field}' field";
                        break;
                }
            }
        }

        return $messages;
    }

    private function response(Response $response, array $messages): void
    {
        $body = $this->getResponseBody(json_encode($messages));
        $response = $response->withHeader('Content-Type', 'application/json')->withBody($body);
        ResponseSender::send($response);
        exit();
    }

    private function getResponseBody(string $body): StreamInterface
    {
        $stream = new Stream('php://temp', 'wb+');
        $stream->write($body);
        $stream->rewind();

        return $stream;
    }
}