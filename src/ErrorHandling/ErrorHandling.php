<?php

declare(strict_types=1);

namespace Src\ErrorHandling;

use Src\Base\BaseView;

class ErrorHandling
{
    public static function errorHandler($severity, $message, $file, $line): void
    {
        if (!(error_reporting() && $severity)) {

            return;
        }

        throw new \ErrorException($message, 0, $severity, $file, $line);
    }

    public static function exceptionHandler($exception): void
    {
        $code = $exception->getCode();
        if ($code != 404) {
            $code = 500;
        }
        http_response_code($code);
        $error = true;
        if ($error) {
            echo "<h1>Fatal Error</h1>>";
            echo "<p>Uncaught exception: ".get_class($exception)."</p>>";
            echo "<p>Message: {$exception->getMessage()}</p>>";
            echo "<p>Stack trace: {$exception->getTraceAsString()}</p>>";
            echo "<p>Thrown in {$exception->getFile()} on line {$exception->getLine()}</p>>";
        } else {
            $errorLog = LOG_DIR . '/' . date('Y-m-d H:i:s') . '.txt';
            ini_set('error_log', $errorLog);
            $message = "Uncaught exception: ".get_class($exception);
            $message .= "with message" . $exception->getMessage();
            $message .= "\nStack trace: " . $exception->getTraceAsString();
            $message .= "\nTrown in " . $exception->getFile() . " on line " . $exception->getLine();
            error_log($message);
            echo (new BaseView())->getTemplate("error/{$code}.html.twig", ['error_message' => $message]);
        }
    }
}