<?php

use Src\Router\Router;

new Router($this->request);

Router::get('/', [App\Controller\ProductController::class, 'index']);
Router::get('/create', [App\Controller\ProductController::class, 'create']);
