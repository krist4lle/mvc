<?php

use Src\Router\Router;

new Router($this->request);

Router::get('/api/products', [App\Controller\ProductController::class, 'products']);
Router::post('/api/store', [App\Controller\ProductController::class, 'store']);
Router::delete('/api/delete', [App\Controller\ProductController::class, 'delete']);
