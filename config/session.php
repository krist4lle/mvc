<?php

return [
    'name' => 'framework',
    'lifetime' => 0,
    'path' => '/',
    'httponly' => true,
    'domain' => '',
    'gc_maxlifetime' => '1800',
    'gc_divisor' => '1000',
    'gc_probability' => '1',
    'cookie_lifetime' => '0',
    'use_cookies' => '1',
    'secure' => false,
];
