<?php

define('ROOT_DIR', realpath('../'));
$autoload = ROOT_DIR . '/vendor/autoload.php';
if (is_file($autoload)) {
    require $autoload;
}

use Src\Application\Application;

$app = new Application(ROOT_DIR);

$app->run()->setSession()->request()->routing()->response();
