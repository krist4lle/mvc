const typeValue = document.getElementById('type_value');
const typeBook = document.getElementById('Book');
const typeDVD = document.getElementById('DVD');
const typeFurniture = document.getElementById('Furniture');
import formValues from './form-values.js'

formValues.type.addEventListener('change', () => {
    switch (formValues.type.value) {
        case 'book':
            typeValue.setAttribute('class', 'd-block');
            typeBook.setAttribute('class', 'd-block');
            typeFurniture.setAttribute('class', 'd-none');
            typeDVD.setAttribute('class', 'd-none');
            break;
        case 'dvd':
            typeValue.setAttribute('class', 'd-block');
            typeDVD.setAttribute('class', 'd-block');
            typeFurniture.setAttribute('class', 'd-none');
            typeBook.setAttribute('class', 'd-none');
            break;
        case 'furniture':
            typeValue.setAttribute('class', 'd-block');
            typeFurniture.setAttribute('class', 'd-block');
            typeDVD.setAttribute('class', 'd-none');
            typeBook.setAttribute('class', 'd-none');
            break;
        default:
            typeValue.setAttribute('class', 'd-none');
            typeFurniture.setAttribute('class', 'd-none');
            typeDVD.setAttribute('class', 'd-none');
            typeBook.setAttribute('class', 'd-none');
    }
});