export default {
    loadProducts() {
        fetch('/api/products', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            method: 'GET',
        })
            .then(response => response.json())
            .then(json => {
                const container = document.getElementById('container');
                const products = document.createElement('div');
                products.setAttribute('class', 'row');
                container.appendChild(products);

                for (let i = 0; i < json.length; i++) {
                    let product = document.createElement('div');
                    product.setAttribute('class', 'col-3 mb-4');
                    products.appendChild(product);

                    let border = document.createElement('div');
                    border.setAttribute('class', 'border');
                    product.appendChild(border);

                    let checkboxDiv = document.createElement('div');
                    checkboxDiv.setAttribute('class', 'p-2');
                    border.appendChild(checkboxDiv);

                    let checkbox = document.createElement('input');
                    checkbox.setAttribute('type', 'checkbox');
                    checkbox.setAttribute('class', 'delete-checkbox');
                    checkbox.setAttribute('value', json[i].id);
                    checkboxDiv.appendChild(checkbox);

                    let productInfoDiv = document.createElement('div');
                    productInfoDiv.setAttribute('class', 'text-center');
                    border.appendChild(productInfoDiv);

                    let sku = document.createElement('p');
                    sku.innerText = 'JVC' + json[i].sku;
                    productInfoDiv.appendChild(sku);
                    let name = document.createElement('p');
                    name.innerText = json[i].name;
                    productInfoDiv.appendChild(name);
                    let price = document.createElement('p');
                    price.innerText = json[i].price + ' $';
                    productInfoDiv.appendChild(price);
                    let typeValue = document.createElement('p');
                    switch (json[i].type_id) {
                        case 1:
                            typeValue.innerText = 'Size: ' + json[i].type_value + ' MB';
                            break;
                        case 2:
                            typeValue.innerText = 'Weight: ' + json[i].type_value + 'KG';
                            break;
                        case 3:
                            typeValue.innerText = 'Dimensions: ' + json[i].type_value;
                            break;
                    }
                    productInfoDiv.appendChild(typeValue);
                }
            })
    },

    removeProducts() {
        while (container.firstChild) {
            container.removeChild(container.lastChild);
        }
    }
}
