const submitButton = document.getElementById('submitButton');
import formValues from './form-values.js'

submitButton.addEventListener('click', (event) => {
    const fetchData = getDataForFetch();

    fetch('/api/store', {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify(fetchData),
    })
        .then(response => response.json())
        .then(json => {
            if (json === 'Product saved') {
                window.location.replace('/');
            }
            deleteAlerts();
            showAlerts(json);
        })
});

function getDataForFetch() {
    const typeValue = getTypeValue();

    let data = {};
    data['sku'] = Number(formValues.sku.value) === 0 ? null : Number(formValues.sku.value);
    data['name'] = formValues.name.value;
    data['price'] = parseFloat(formValues.price.value);
    data['type'] = formValues.type.value;
    data['type_value'] = Number(typeValue);

    return data;
}

function getTypeValue() {
    const dimensions = getDimensions();
    let typeValue = null;

    switch (false) {
        case !formValues.weight.value:
            typeValue = formValues.weight.value;
            break;
        case !formValues.size.value:
            typeValue = formValues.size.value;
            break;
        case !dimensions:
            typeValue = dimensions;
            break;
    }

    return typeValue;
}

function getDimensions() {
    const height = formValues.height.value;
    const width = formValues.width.value;
    const length = formValues.length.value;

    const dimensions = height + width + length + height.length + width.length + length.length;
    const dimensionsNumber = Number(dimensions);
    const dimensionsNumberLength = digitsCount(dimensionsNumber);

    return dimensions.length === dimensionsNumberLength ? dimensionsNumber : 'dimensions';
}

function showAlerts(json) {
    for (let field in json) {
        let alert = document.getElementById('alert_' + field)
        alert.setAttribute('class', 'd-none');
        if (json[field]) {
            alert.setAttribute('class', 'd-block alert alert-danger');
            for (let error in json[field]) {
                let p = document.createElement('p');
                p.innerText = json[field][error];
                alert.appendChild(p);
            }
        }
    }
}

function deleteAlerts() {
    const form = document.getElementById('product_form');
    for (let i = 0; i < form.childElementCount; i++) {
        for (let j = 0; j < form.children.item(i).childElementCount; j++) {
            let alertId = form.children.item(i).children[j].getAttribute('id');
            if (alertId !== null && alertId.startsWith('alert_')) {
                let alert = form.children.item(i).children[j];
                while (alert.firstChild) {
                    alert.removeChild(alert.firstChild);
                }
                alert.setAttribute('class', 'd-none');
            }
        }
    }
}

function digitsCount(n) {
    let count = 0;
    if (n >= 1) ++count;

    while (n / 10 >= 1) {
        n /= 10;
        ++count;
    }

    return count;
}





