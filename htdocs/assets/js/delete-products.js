const deleteButton = document.getElementById('delete-product-btn');
let items = document.getElementsByClassName('delete-checkbox');

import getProducts from './get-products.js';
getProducts.loadProducts();

deleteButton.addEventListener('click', (event) => {
    let checked = [];
    for (let i = 0; i < items.length; i++) {
        let item = items.item(i);
        if (item.checked) {
            checked.push(Number(item.value));
        }
    }

    fetch('/api/delete', {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        method: 'DELETE',
        body: JSON.stringify(checked),
    })
        .then(response => response.json())
        .then(json => {
            getProducts.removeProducts();
            getProducts.loadProducts();
        });
})

