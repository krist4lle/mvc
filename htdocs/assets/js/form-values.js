const sku = document.getElementById('sku');
const name = document.getElementById('name');
const price = document.getElementById('price');
const type = document.getElementById('productType');
const weight = document.getElementById('weight');
const size = document.getElementById('size');
const height = document.getElementById('height');
const width = document.getElementById('width');
const length = document.getElementById('length');

export default {
    sku,
    name,
    price,
    type,
    weight,
    size,
    height,
    width,
    length,
}