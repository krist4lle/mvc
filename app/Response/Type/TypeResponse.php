<?php

declare(strict_types=1);

namespace App\Response\Type;

use Src\Http\Response\ApiResponse;

class TypeResponse extends ApiResponse
{
    public function requiredData(): array
    {
        return [
            'id',
            'name',
            'slug',
        ];
    }
}