<?php

declare(strict_types=1);

namespace App\Response\Product;

use Src\Http\Response\ApiResponse;

class IndexResponse extends ApiResponse
{
    public function requiredData(): array
    {
        return [
            'id',
            'sku',
            'name',
            'price',
            'type_id',
            'type_value'
        ];
    }
}