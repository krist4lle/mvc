<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Product;
use App\Model\Type;

class ProductService
{
    public function store(array $data): void
    {
        if ($data['type'] === 'furniture') {
            $data['type_value'] = $this->parseValue($data['type_value']);
        }

        $type = (new Type())->query()->where('slug', $data['type'])->first();
        $data = $this->changeKey($data, 'type', 'type_id');
        $data['type_id'] = $type->id;

        $product = new Product();
        $product->create($data);
    }

    public function delete($data): void
    {
        for ($i = 0; $i < count($data); $i++) {
            $product = new Product();
            $product = $product->query()->find($data[$i]);
            if (!isset($product->id)) {
                throw new \InvalidArgumentException("Product doesn't exist", 400);
            }
            $product->delete();
        }
    }

    private function parseValue(int $value): string
    {
        $value = (string) $value;

        $key = substr($value, -3, 3);
        $heightKey = (int) substr($key, 0, 1);
        $widthKey = (int) substr($key, 1, 1);
        $lengthKey = (int) substr($key, 2, 1);

        $height = substr($value, 0, $heightKey);
        $width = substr($value, $heightKey, $widthKey);
        $length = substr($value, ($heightKey + $widthKey), $lengthKey);

        return "{$height}x{$width}x{$length}";
    }

    private function changeKey(array $data, string $oldKey, string $newKey): array
    {
        $keys = array_keys($data);
        $keys[array_search($oldKey, $keys)] = $newKey;

        return array_combine($keys, $data);
    }
}