<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Product;
use App\Model\Type;
use App\Requests\Product\StoreRequest;
use App\Response\Product\IndexResponse;
use App\Response\Type\TypeResponse;
use App\Service\ProductService;
use Src\Base\BaseController;
use Src\Http\Request\ServerRequestInterface;

class ProductController extends BaseController
{
    public function index()
    {
        return $this->view('products/index');
    }

    public function create()
    {
        $types = (new Type())->query()->get();
        $types = (new TypeResponse())->collection($types);

        return $this->view('products/create', ['types'=> $types]);
    }

    public function products()
    {
        $products = (new Product())->query()->get();

        return (new IndexResponse())->collection($products);
    }

    public function store()
    {
        $request = new StoreRequest();
        $data = $request->validated();
        $service = new ProductService();
        $service->store($data);

        return 'Product saved';
    }

    public function delete(ServerRequestInterface $request)
    {
        $data = json_decode($request->getBody()->getContents());
        $service = new ProductService();
        $service->delete($data);

        return 'Product deleted';
    }
}