<?php

declare(strict_types=1);

namespace App\Requests\Product;

use Src\Http\Request\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'sku' => 'required|number|unique=products,sku',
            'name' => 'required|string|alpha',
            'price' => 'required|number',
            'type' => 'required|string|alpha|exists=types,slug',
            'type_value' => 'required|number',
        ];
    }
}