<header class="oleez-header">
    <nav class="navbar navbar-expand justify-content-between">
        <div>
            <h1>Product Add</h1>
        </div>
        <div>
            <ul class="navbar-nav d-none d-lg-flex">
                <li class="nav-item px-2">
                    <button id="submitButton">Save</button>
                </li>
                <li class="nav-item px-2">
                    <a href="/">
                        <button>Cancel</button>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<hr>
<main>
    <div class="container-fluid">
        <div class="row px-5">
            <form id="product_form" class="w-25" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="sku">SKU*</label>
                    <div id="alert_sku" class="d-none"></div>
                    <input id="sku" name="sku" type="number" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="name">Name*</label>
                    <div id="alert_name" class="d-none"></div>
                    <input id="name" name="name" type="text" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="price">Price ($)*</label>
                    <div id="alert_price" class="d-none"></div>
                    <input id="price" name="price" type="text" placeholder="100.00" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="productType">Type Switcher*</label>
                    <div id="alert_type" class="d-none"></div>
                    <select id="productType" name="type" class="form-control">
                        <option>Open this select menu</option>
                        <?php foreach ($types as $type): ?>
                            <option value="<?php echo $type->slug ?>"><?php echo $type->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div id="type_value" class="mb-3 d-none">
                    <div id="alert_type_value" class="d-none"></div>
                    <div class="mb-3 d-none" id="Book">
                        <label for="weight">Weight (KG)*</label>
                        <input id="weight" name="weight" type="text" class="form-control">
                        <p>Please, provide weight in KG</p>
                    </div>
                    <div class="mb-3 d-none" id="DVD">
                        <label for="size">Size (MB)*</label>
                        <input id="size" name="size" type="text" class="form-control">
                        <p>Please, provide size in MB</p>
                    </div>
                    <div class="mb-3 d-none" id="Furniture">
                        <label for="height">Height (CM)*</label>
                        <input id="height" name="height" type="text" class="form-control">
                        <label for="width">Width (CM)*</label>
                        <input id="width" name="width" type="text" class="form-control">
                        <label for="length">Length (CM)*</label>
                        <input id="length" name="length" type="text" class="form-control">
                        <p>Please, provide dimensions in CM</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
<script type="module" src="assets/js/type-switcher.js"></script>
<script type="module" src="assets/js/submit-form.js"></script>