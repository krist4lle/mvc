<header class="oleez-header">
    <nav class="navbar navbar-expand justify-content-between">
        <div>
            <h1>Product List</h1>
        </div>
        <div>
            <ul class="navbar-nav d-none d-lg-flex">
                <li class="nav-item px-2">
                    <a href="/create"><button>ADD</button></a>
                </li>
                <li class="nav-item px-2">
                    <button id="delete-product-btn">MASS DELETE</button>
                </li>
            </ul>
        </div>
    </nav>
</header>
<hr>
<main class="blog-grid-page">
    <div id="container" class="container"></div>
</main>
<script type="module" src="assets/js/delete-products.js"></script>
